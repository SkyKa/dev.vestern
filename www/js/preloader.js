var game = new Phaser.Game(992, 510, Phaser.CANVAS, 'the-game', { preload: 
preload, create: create, render: render });


/*game.state.add('MainMenu',Game.MainMenu);*/

function preload() {
	this.load.image('background', 'assets/images/background.jpg');
	this.load.image('gun' , 'assets/images/gun.png');
	this.load.image('cap' , 'assets/images/cap.png');
	this.load.image('button' , 'assets/images/start_b.png');
}

var sprite;
var timer;
var total = 0;
var gun;

function create() {

	game.physics.startSystem(Phaser.Physics.ARCADE);
	
	
	this.background = this.game.add.sprite(0, 0, 'background');

    gun = this.gun = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'gun');
	this.gun.anchor.setTo(0.5, -0.2);
	this.gun.scale.setTo(0.7, 0.7);

    game.time.events.repeat(Phaser.Timer.SECOND * 2, 2, createBall, this);

}

function createBall() {

	sprite = game.add.sprite(-100, 255, 'cap', 2);

	game.physics.arcade.enable(sprite);

	game.physics.arcade.gravity.x = 200;

	sprite.inputEnabled = true;

	sprite.events.onInputDown.add(destroySprite, this);

}


function update() {


}

function destroySprite (sprite) {

    sprite.destroy();

}

function render() {

}