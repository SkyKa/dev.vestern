var game = new Phaser.Game(1300, 510, Phaser.CANVAS, 'the-game', { preload: 
preload, create: create, render: render });

function preload() {

		this.load.image('background', 'assets/images/background_game.jpg');
		this.load.image('gun' , 'assets/images/gun.png');
		this.load.image('cap' , 'assets/images/cap.png');
        this.load.spritesheet('smoke', 'assets/images/smoke-sprite.png', 116, 100, 14);
		this.load.image('button' , 'assets/images/start_b.png');
		this.load.image('patron1' , 'assets/images/patron.png');
		this.load.image('patron2' , 'assets/images/patron.png');
		this.load.image('krug' , 'assets/images/krug.png');
		this.load.audio('shot3', ['/mp3/shot3.mp3']);
		this.load.audio('perezaryad', ['/mp3/perezaryad.mp3']);
		this.load.audio('shot_empty', ['/mp3/shot_empty.mp3']);

}

var sprite;
var timer;
var gun;
var win;
var tryagain;
var score_text;
var patron1;
var patron2;
var krug;
var chet = 0;
var shot3;
var perezaryad;
var tween;
var vistreli = 2;
var shot_empty;
var score = 0;
var plusscore;
var game_check = true;


function create() {

	game.physics.startSystem(Phaser.Physics.ARCADE);

	this.background = this.game.add.sprite(0, 0, 'background');

    gun = this.gun = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY + 350, 'gun');
	this.gun.anchor.setTo(0.5, 0.5);
	this.gun.scale.setTo(0.7);

	game.input.mouse.capture = true;

    game.time.events.repeat(Phaser.Timer.SECOND * 2, 1, createBall_1, this);

    krug = this.krug = this.game.add.sprite(70, 450, 'krug');
    this.krug.anchor.setTo(0.5);

    patron1 = this.patron1 = this.game.add.sprite(60, 450, 'patron1');
    this.patron1.anchor.setTo(0.5);

    patron2 = this.patron2 = this.game.add.sprite(80, 450, 'patron2');
    this.patron2.anchor.setTo(0.5);

    tryagain = game.add.text(1180, 435 , 'ДАЙ!', {font: '60px bebas-bold' , fill:'#000000'});
    tryagain.inputEnabled = true;
    tryagain.events.onInputDown.add(trythis, this);
    tryagain.input.useHandCursor = true;

    score_text = game.add.text(10, 0 , 'Score: ' + score,
    {font: '60px bebas-bold' , fill:'#000000'});

}

function trythis(){
    game.paused = true;
    game.paused = false;
    if (game_check == true){
    game.time.events.repeat(Phaser.Timer.SECOND * 2, 1, createBall_1);}
    if (patron1.alive == false && patron2.alive == false) {
    patron1 = this.patron1 = this.game.add.sprite(60, 450, 'patron1');
    this.patron1.anchor.setTo(0.5);
    patron2 = this.patron2 = this.game.add.sprite(80, 450, 'patron2');
    this.patron2.anchor.setTo(0.5);
    }

    if (patron1.alive == true && patron2.alive == false) {
    patron2 = this.patron2 = this.game.add.sprite(80, 450, 'patron2');
    this.patron2.anchor.setTo(0.5);
    }

    vistreli = 2;

    game_check = false;
}

function play_m() {
	shot3 = game.add.audio('shot3');
	shot3.play();

	if (vistreli == 2 ){
		 patron2.destroy();
	}
		if (vistreli == 1 ){
		 patron1.destroy();
	}

}

function reload_g() {
	perezaryad = game.add.audio('perezaryad');
	perezaryad.play();

	document.addEventListener("contextmenu", function(e){ 
	e.preventDefault(); 
	}, false);

	if (patron1.alive == false && patron2.alive == false) {
	patron1 = this.patron1 = this.game.add.sprite(60, 450, 'patron1');
    this.patron1.anchor.setTo(0.5);
    patron2 = this.patron2 = this.game.add.sprite(80, 450, 'patron2');
    this.patron2.anchor.setTo(0.5);
    }

	if (patron1.alive == true && patron2.alive == false) {
    patron2 = this.patron2 = this.game.add.sprite(80, 450, 'patron2');
    this.patron2.anchor.setTo(0.5);
    }

}

function empty() {
	shot_empty = game.add.audio('shot_empty');
	shot_empty.play();
}


function createBall_1() {

	var rand = game.rnd.integerInRange(1, 5);

if (rand == 1){
	sprite = game.add.sprite(0, 450, 'cap');
    game.physics.arcade.enable(sprite);
	sprite.inputEnabled = true;
	sprite.events.onInputDown.add(destroySprite, this);
    sprite.checkWorldBounds = true;
    sprite.outOfBoundsKill = true;
    tween = game.add.tween(sprite).to( { x: [650, 1400 ], y: [ -250, 560 ] }, 3000, 
    	"Sine.easeInOut", true, -1, false);
    tween.interpolation(Phaser.Math.bezierInterpolation);
        
 }

if (rand == 2){
	sprite = game.add.sprite(0, 255, 'cap');
	game.physics.arcade.enable(sprite);
	sprite.inputEnabled = true;
	sprite.events.onInputDown.add(destroySprite, this);
    sprite.checkWorldBounds = true;
    sprite.outOfBoundsKill = true;
    tween = game.add.tween(sprite).to( { x: [1400, 1400], y: [ 255, 255 ] }, 6000, 
    	"Sine.easeInOut", true, -1, false);
    tween.interpolation(Phaser.Math.linearInterpolation);
            
  }

 if (rand == 3){
	sprite = game.add.sprite(0, 450, 'cap');
	game.physics.arcade.enable(sprite);
	sprite.inputEnabled = true;
	sprite.events.onInputDown.add(destroySprite, this);
    sprite.checkWorldBounds = true;
    sprite.outOfBoundsKill = true;
    tween = game.add.tween(sprite).to( { x: [2400, 2400 ], y: [ -510, -510 ] }, 6000, 
    	"Sine.easeInOut", true, -1, false);
    tween.interpolation(Phaser.Math.linearInterpolation);
            
 }

if (rand == 4){
	sprite = game.add.sprite(0, 0, 'cap');
	game.physics.arcade.enable(sprite);
	sprite.inputEnabled = true;
	sprite.events.onInputDown.add(destroySprite, this);
    sprite.checkWorldBounds = true;
    sprite.outOfBoundsKill = true;
    tween = game.add.tween(sprite).to( { x: [2400, 2400], y: [ 910, 910 ] }, 6000, 
    	"Sine.easeInOut", true, -1, false);
    tween.interpolation(Phaser.Math.linearInterpolation);
            
}

if (rand == 5){
	sprite = game.add.sprite(0, 450, 'cap');
	game.physics.arcade.enable(sprite);
	sprite.inputEnabled = true;
	sprite.events.onInputDown.add(destroySprite, this);
    sprite.checkWorldBounds = true;
    sprite.outOfBoundsKill = true;
    tween = game.add.tween(sprite).to( { x: [ 650, 1400 ], y: [ -410, 510 ] }, 3000, 
    	"Sine.easeInOut", true, -1, false);
    tween.interpolation(Phaser.Math.bezierInterpolation);
        
}

    game.time.events.repeat(Phaser.Timer.SECOND * 2, 1, createBall_2);
    game_check == true;
}

function createBall_2() {

	var rand2 = game.rnd.integerInRange(1, 5);

if (rand2 == 1){
	sprite = game.add.sprite(1300, 450, 'cap');
	game.physics.arcade.enable(sprite);
	sprite.inputEnabled = true;
	sprite.events.onInputDown.add(destroySprite, this);
    sprite.checkWorldBounds = true;
    sprite.outOfBoundsKill = true;
    tween = game.add.tween(sprite).to( { x: [650, -100 ], y: [ -250, 560 ] }, 3000, 
    	"Sine.easeInOut", true, -1, false);
    tween.interpolation(Phaser.Math.bezierInterpolation);
            
 }

if (rand2 == 2){
	sprite = game.add.sprite(1300, 255, 'cap');
	game.physics.arcade.enable(sprite);
	sprite.inputEnabled = true;
	sprite.events.onInputDown.add(destroySprite, this);
    sprite.checkWorldBounds = true;
    sprite.outOfBoundsKill = true;
    tween = game.add.tween(sprite).to( { x: [-1400, -100], y: [ 255, 255 ] }, 6000, 
    	"Sine.easeInOut", true, -1, false);
    tween.interpolation(Phaser.Math.linearInterpolation);
            
  }

 if (rand2 == 3){
	sprite = game.add.sprite(1200, 450, 'cap');
	game.physics.arcade.enable(sprite);
	sprite.inputEnabled = true;
	sprite.events.onInputDown.add(destroySprite, this);
    sprite.checkWorldBounds = true;
    sprite.outOfBoundsKill = true;
    tween = game.add.tween(sprite).to( { x: [-2600, -1400 ], y: [ -510, -510 ] }, 6000, 
    	"Sine.easeInOut", true, -1, false);
    tween.interpolation(Phaser.Math.linearInterpolation);
            
 }

if (rand2 == 4){
	sprite = game.add.sprite(1300, 0, 'cap');
	game.physics.arcade.enable(sprite);
	sprite.inputEnabled = true;
	sprite.events.onInputDown.add(destroySprite, this);
    sprite.checkWorldBounds = true;
    sprite.outOfBoundsKill = true;
    tween = game.add.tween(sprite).to( { x: [-2400, -2400], y: [ 1310, 1310 ] }, 6000, 
    	"Sine.easeInOut", true, -1, false);
    tween.interpolation(Phaser.Math.linearInterpolation);
            
}

if (rand2 == 5){
	sprite = game.add.sprite(1300, 450, 'cap');
	game.physics.arcade.enable(sprite);
	sprite.inputEnabled = true;
	sprite.events.onInputDown.add(destroySprite, this);
    sprite.checkWorldBounds = true;
    sprite.outOfBoundsKill = true;
    tween = game.add.tween(sprite).to( { x: [ 650, -100 ], y: [ -410, 510 ] }, 3000, 
    	"Sine.easeInOut", true, -1, false);
    tween.interpolation(Phaser.Math.bezierInterpolation);
        
}

}


function destroySprite() {
	if (vistreli > 0){
    smoke = game.add.sprite(sprite.x, sprite.y, 'smoke');
    smoke.animations.add('walk');
    smoke.animations.play('walk', 10, true);
    sprite.destroy();
    chet++;
    score += 100;
    score_text.destroy();
    score_text = game.add.text(10, 0 , 'Score: ' + score,
    {font: '60px bebas-bold' , fill:'#000000'});

    plusscore = game.add.text(sprite.x, sprite.y , '+100',
    {font: '30px bebas-bold' , fill:'#000000'});
    tween = game.add.tween(plusscore).to( { y: [ -200, -200 ] }, 6000, 
        "Sine.easeInOut", true, -1, false);
    tween.interpolation(Phaser.Math.linearInterpolation);
    game.time.events.repeat(Phaser.Timer.SECOND * 1, 1, capboom, this);

    }

    if (chet == 2) {
        $( "#skidka" ).trigger( "click" );
        chet += 1;
    }

}

function capboom() {
    smoke.destroy();
    plusscore.destroy();
}



function render() {

	if (game.input.activePointer.leftButton.isDown == true && vistreli > 0) {
		play_m();
		game.input.activePointer.leftButton.isDown = false;
		vistreli--;		
	}

	if (game.input.activePointer.leftButton.isDown == true && vistreli == 0) {
		empty();
		game.input.activePointer.leftButton.isDown = false;
		vistreli = 0;		
	}

	if (game.input.activePointer.rightButton.isDown == true ) {
		reload_g();
		game.input.activePointer.rightButton.isDown = false;
		vistreli = 2;
	}

    if (game.input.mousePointer.x > 400 && game.input.mousePointer.x < 900){
     gun.rotation = game.physics.arcade.angleToPointer(gun);
     }


}
